#!/bin/sh

sleep 10
#apk add --no-cache sudo
#apt-get update && apt-get install -y --no-install-recommends sudo
cd /var/www/html
echo 'Configure OCC'
sudo -u www-data php occ --no-warnings app:install onlyoffice
sudo -u www-data php occ app:enable onlyoffice
sudo -u www-data php occ --no-warnings db:add-missing-indices
sudo -u www-data php occ --no-warnings db:convert-filecache-bigint
sudo -u www-data php occ --no-warnings config:system:set onlyoffice DocumentServerUrl --value="/ds-vpath/"
sudo -u www-data php occ --no-warnings config:system:set onlyoffice DocumentServerInternalUrl --value="http://onlyoffice-document-server/"
sudo -u www-data php occ --no-warnings config:system:set onlyoffice StorageUrl --value="http://nginx-server/"
sudo -u www-data php occ --no-warnings config:system:set allow_local_remote_servers  --value=true

echo 'Configure TrustedDomains'
sudo -u www-data php occ --no-warnings config:system:get trusted_domains >> trusted_domain.tmp

if ! grep -q "nginx-server" trusted_domain.tmp; then
    TRUSTED_INDEX=$(cat trusted_domain.tmp | wc -l);
    sudo -u www-data php occ --no-warnings config:system:set trusted_domains $TRUSTED_INDEX --value="nginx-server"
fi

rm trusted_domain.tmp

exec php-fpm


echo 'END CONFIGURE'
